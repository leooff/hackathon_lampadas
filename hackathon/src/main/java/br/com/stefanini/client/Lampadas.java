package br.com.stefanini.client;

import java.net.URI;

import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class Lampadas {

	private static final String URL_BASE = "http://10.14.11.28/Hack/api";

	private static String TOKEN = "lobisomem";
	private static RestTemplate restTemplate = new RestTemplate();

	// CONSULTAR STATUS GERAL
	public static Lampada[] StatusLampadas() {
		ResponseEntity<Lampada[]> response = null;
		try {
			response = restTemplate
					.exchange(RequestEntity.get(new URI(URL_BASE + "/lampadas")).build(), Lampada[].class);

			if (!response.getStatusCode().is2xxSuccessful()) {
				System.out.println("Erro na Requisição: Status:" + response.getStatusCodeValue());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return response.getBody();
	}

	// ACENDER LAMPADA
	public static void alterarStatusLampada(int lampada, boolean status) {
		try {
			Lampada lampadaEntity = new Lampada(lampada, status);

			ResponseEntity<String> response = restTemplate
					.exchange(RequestEntity.post(new URI(URL_BASE + "/lampadas/Post?token=" + TOKEN))
							.contentType(MediaType.APPLICATION_JSON).body(lampadaEntity), String.class);

			if (!response.getStatusCode().is2xxSuccessful()) {
				System.out.println("Erro na Requisição: Status:" + response.getStatusCodeValue());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
