package br.com.stefanini.client;

public class Lampada {
	public int lampada;
	public boolean status;
	
	public Lampada() {
		
	}
	
	public Lampada(int lampada, boolean status) {
		this.lampada = lampada;
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "[Lampada:"+lampada + ", Status:"+status+"]";
	}
}
